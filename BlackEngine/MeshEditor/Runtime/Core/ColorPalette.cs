using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine.Serialization;

namespace BlackEngine.MeshEditor
{
    [Serializable]
    sealed class ColorPalette : ScriptableObject, IHasDefault
    {
        public Color current { get; set; }

        [SerializeField] List<Color> m_Colors;//colors
        public ReadOnlyCollection<Color> colors => new ReadOnlyCollection<Color>(m_Colors);
        public void SetColors(IEnumerable<Color> colors)
        {
            if (colors == null) throw new ArgumentNullException("colors");
            m_Colors = colors.ToList();
        }
        public void SetDefaultValues()
        {
            m_Colors = new List<Color>()
            {
                new Color(0.000f, 0.122f, 0.247f, 1f),
                new Color(0.000f, 0.455f, 0.851f, 1f),
                new Color(0.498f, 0.859f, 1.000f, 1f),
                new Color(0.224f, 0.800f, 0.800f, 1f),
                new Color(0.239f, 0.600f, 0.439f, 1f),
                new Color(0.180f, 0.800f, 0.251f, 1f),
                new Color(0.004f, 1.000f, 0.439f, 1f),
                new Color(1.000f, 0.863f, 0.000f, 1f),
                new Color(1.000f, 0.522f, 0.106f, 1f),
                new Color(1.000f, 0.255f, 0.212f, 1f),
                new Color(0.522f, 0.078f, 0.294f, 1f),
                new Color(0.941f, 0.071f, 0.745f, 1f),
                new Color(0.694f, 0.051f, 0.788f, 1f),
                new Color(0.067f, 0.067f, 0.067f, 1f),
                new Color(0.667f, 0.667f, 0.667f, 1f),
                new Color(0.867f, 0.867f, 0.867f, 1f)
            };
        }
        public Color this[int i]
        {
            get => m_Colors[i];
            set => m_Colors[i] = value;
        }
        public int Count => m_Colors.Count;
    }
}

﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Input;

namespace BlackEngine.CharacterController
{
    public class FollowCamera : CoreEngine
    {
        public Transform cameraTarget;

        [Tooltip("Distance (Min, Default, Max)")]
        public Vector3 distance = new Vector3(2, 6, 10);

        [Tooltip("Scroll (ZoomIn, delta)")]
        public Vector2 scrollDistance = new Vector2(5, 10);

        [Tooltip("Limit Angle (Min, Default, Max)")]
        public Vector3 verticalAngle = new Vector3(-80, 20, 80);

        [Space(5)]
        public bool invertX = false;
        public bool invertY = false;
        public float sensitivity = 2;

        [Space(5)]
        public Vector2 pointOffset = new Vector2(0f, 0f);
        public float deltaRotation = 30f;
        public float deltaSpeed = 30f;

        [Header("Obstruction")]
        [Tooltip("checkRadius, delta")]
        public Vector2 obstruction = new Vector2(.5f, 10000);
        public LayerMask obstructionLayers = -1;

        //Private
        private float _targetDistance;
        private Vector3 _planarDirection;
        private Camera _camera;
        private List<Collider> _internalIgnoredColliders = new List<Collider>();
        private bool _distanceIsObstructed;
        private float _targetVerticalAngle;
        private RaycastHit _obstructionHit;
        private int _obstructionCount;
        private RaycastHit[] _obstructions = new RaycastHit[MaxObstructions];
        private float _obstructionTime;
        private Vector3 _currentFollowPosition;

        private const int MaxObstructions = 32;

        void OnValidate() // Unity Editor
        {
            distance.y = Mathf.Clamp(distance.y, distance.x, distance.z);
            verticalAngle.y = Mathf.Clamp(verticalAngle.y, verticalAngle.x, verticalAngle.z);
            verticalAngle.x = Mathf.Clamp(verticalAngle.x, -90, 90);
            verticalAngle.z = Mathf.Clamp(verticalAngle.z, -90, 90);
            scrollDistance.x = Mathf.Clamp(scrollDistance.x, 0, float.MaxValue);
            scrollDistance.y = Mathf.Clamp(scrollDistance.y, 0, float.MaxValue);
        }

        void Awake()
        {
            _camera = GetComponent<Camera>();
            _targetDistance = distance.y;
            _targetVerticalAngle = verticalAngle.y;
            _planarDirection = transform.forward;//Vector3.forward;
            _currentFollowPosition = cameraTarget.position;
            _internalIgnoredColliders.Clear();
        }

        public void SetIgnoredColliders(IEnumerable<Collider> colliders) => _internalIgnoredColliders.AddRange(colliders); //GetComponentsInChildren<Collider>()
        public void UpdateControl(float deltaTime, float zoomInput, Vector2 rotationInput)
        {
            if (cameraTarget)
            {
                if(Cursor.lockState != CursorLockMode.Locked) rotationInput = Vector3.zero;
                if (invertX) rotationInput.x *= -1f;
                if (invertY) rotationInput.y *= -1f;

                // Process rotation input
                Quaternion rotationFromInput = Quaternion.Euler(cameraTarget.up * (rotationInput.x * sensitivity));
                _planarDirection = rotationFromInput * _planarDirection;
                _planarDirection = Vector3.Cross(cameraTarget.up, Vector3.Cross(_planarDirection, cameraTarget.up));
                _targetVerticalAngle -= (rotationInput.y * sensitivity);
                _targetVerticalAngle = Mathf.Clamp(_targetVerticalAngle, verticalAngle.x, verticalAngle.z);

                // Process distance input
                if (_distanceIsObstructed && Mathf.Abs(zoomInput) > 0f) _targetDistance = distance.y;
                _targetDistance += zoomInput * scrollDistance.x;
                _targetDistance = Mathf.Clamp(_targetDistance, distance.x, distance.z);

                // Find the smoothed follow position
                _currentFollowPosition = Vector3.Lerp(_currentFollowPosition, cameraTarget.position, 1f - Mathf.Exp(-deltaSpeed * deltaTime));

                // Calculate smoothed rotation
                Quaternion planarRot = Quaternion.LookRotation(_planarDirection, cameraTarget.up);
                Quaternion verticalRot = Quaternion.Euler(_targetVerticalAngle, 0, 0);
                Quaternion targetRotation = Quaternion.Slerp(transform.rotation, planarRot * verticalRot, 1f - Mathf.Exp(-deltaRotation * deltaTime));

                // Apply rotation
                transform.rotation = targetRotation;

                ObstructionUpdate(deltaTime);

                // Find the smoothed camera orbit position
                Vector3 targetPosition = _currentFollowPosition - ((targetRotation * Vector3.forward) * distance.y);

                // Handle framing
                targetPosition += transform.right * pointOffset.x;
                targetPosition += transform.up * pointOffset.y;

                // Apply position
                transform.position = targetPosition;
            }
        }
        public void UpdateControl() 
        {
            UpdateControl(Time.deltaTime, -Mathf.Clamp(Mouse.current.scroll.ReadValue().y, -1, 1), Mouse.current.delta.ReadValue());
        }

        private void ObstructionUpdate(float deltaTime)
        {
            RaycastHit closestHit = new RaycastHit();
            closestHit.distance = Mathf.Infinity;
            _obstructionCount = Physics.SphereCastNonAlloc(_currentFollowPosition, obstruction.x, -transform.forward, _obstructions, _targetDistance, obstructionLayers, QueryTriggerInteraction.Ignore);
            for (int i = 0; i < _obstructionCount; i++)
            {
                bool isIgnored = false;
                for (int j = 0; j < _internalIgnoredColliders.Count; j++)
                {
                    if (_internalIgnoredColliders[j] == _obstructions[i].collider)
                    {
                        isIgnored = true;
                        break;
                    }
                }
                // for (int j = 0; j < CharacterController.IgnoredColliders.Count; j++)
                // {
                //     if (CharacterController.IgnoredColliders[j] == _obstructions[i].collider)
                //     {
                //         isIgnored = true;
                //         break;
                //     }
                // }

                if (!isIgnored && _obstructions[i].distance < closestHit.distance && _obstructions[i].distance > 0) closestHit = _obstructions[i];
            }

            if (closestHit.distance < Mathf.Infinity) // If obstructions detecter
            {
                _distanceIsObstructed = true;
                distance.y = Mathf.Lerp(distance.y, closestHit.distance, 1 - Mathf.Exp(-obstruction.y * deltaTime));
            }
            else // If no obstruction
            {
                _distanceIsObstructed = false;
                distance.y = Mathf.Lerp(distance.y, _targetDistance, 1 - Mathf.Exp(-scrollDistance.y * deltaTime));
            }
        }
    }

}

using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.ObjectModel;
using UnityEngine;
using BlackEngine.MeshEditor.Runtime;

namespace BlackEngine.MeshEditor
{
    public class MeshInspectorProperty : CoreEngine
    {
        [Flags]
        private protected enum CacheValidState : byte
        {
            SharedVertex = 1 << 0,
            SharedTexture = 1 << 1
        }

        private protected CacheValidState m_CacheValid;
        private protected List<Vector4> m_Textures2;//_uv3
        private protected List<Vector4> m_Textures3;//_uv4
        public bool userCollisions { get; set; }

        //m_SharedTextureLookup
        public Dictionary<int, int> sharedTextureLookup
        {
            get
            {
                if ((m_CacheValid & CacheValidState.SharedTexture) != CacheValidState.SharedTexture)
                {
                    m_CacheValid |= CacheValidState.SharedTexture;
                    SharedVertex.GetSharedVertexLookup(m_SharedTextures, m_SharedTextureLookup);
                }
                return m_SharedTextureLookup;
            }
        }
        private protected Dictionary<int, int> m_SharedTextureLookup;

        //m_SharedTextures
        public SharedVertex[] sharedTextures
        {
            get => m_SharedTextures;
            set
            {
                m_SharedTextures = value;
                InvalidateSharedTextureLookup();
            }
        }
        private protected virtual void InvalidateSharedTextureLookup()
        {
            throw new NotImplementedException();
        }
        private protected SharedVertex[] m_SharedTextures;//_sharedIndicesUV

        //m_Textures0
        public Vector2[] textures
        {
            get => m_Textures0 != null ? m_Textures0 : null;
            set
            {
                if (value == null) m_Textures0 = null;
                else if (value.Count() != vertexCount) throw new ArgumentOutOfRangeException("value");
                else m_Textures0 = value.ToArray();
            }
        }
        private protected Vector2[] m_Textures0;//_uv

        //m_Tangents
        public Vector4[] tangents
        {
            get => m_Tangents == null || m_Tangents.Length != vertexCount ? null : m_Tangents;
            set
            {
                if (value == null) m_Tangents = null;
                else if (value.Count() != vertexCount) throw new ArgumentOutOfRangeException("value", "Tangent array length must match vertex count");
                else m_Tangents = value.ToArray();
            }
        }
        private Vector4[] m_Tangents;//_tangents

        //m_Normals
        public Vector3[] normals
        {
            get => m_Normals;
            set => m_Normals = value;
        }
        private Vector3[] m_Normals;

        //m_SharedVertices
        public SharedVertex[] sharedVertices
        {
            get => m_SharedVertices;
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                int len = value.Length;
                m_SharedVertices = new SharedVertex[len];
                for (var i = 0; i < len; i++) m_SharedVertices[i] = new SharedVertex(value[i]);
                InvalidateSharedVertexLookup();
                //m_SharedVertices = value;
                //InvalidateSharedVertexLookup();
            }
        }
        private protected virtual void InvalidateSharedVertexLookup()
        {
            throw new NotImplementedException();
        }
        private protected SharedVertex[] m_SharedVertices;//_sharedIndices

        //m_Colors
        public Color[] colors
        {
            get => m_Colors != null ? m_Colors : null;
            set
            {
                if (value == null) m_Colors = null;
                else if (value.Count() != vertexCount) throw new ArgumentOutOfRangeException("value", "Array length must match vertex count.");
                else m_Colors = value.ToArray();
            }
        }
        private protected Color[] m_Colors;//_colors

        //m_VerticesPositions
        public Vector3[] verticesPositions
        {
            get => m_VerticesPositions;
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                m_VerticesPositions = value;
            }
        }
        private protected Vector3[] m_VerticesPositions;//_vertices

        //m_Faces
        public Face[] faces
        {
            get => m_Faces;
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                m_Faces = value.ToArray();
            }
        }
        private protected Face[] m_Faces;//_quads

        //m_UnwrapParameters
        public UnwrapParameters unwrapParameters
        {
            get { return m_UnwrapParameters; }
            set { m_UnwrapParameters = value; }
        }
        UnwrapParameters m_UnwrapParameters;//unwrapParameters

        //m_IsSelectable
        public bool selectable
        {
            get => m_IsSelectable;
            set => m_IsSelectable = value;
        }
        bool m_IsSelectable = true;

        //m_SelectedFaces
        public int selectedFaceCount => m_SelectedFaces.Length;
        int[] m_SelectedFaces = new int[] { };

        //m_SelectedVertices
        public int selectedVertexCount => m_SelectedVertices.Length;
        int[] m_SelectedVertices = new int[] { };

        //m_SelectedEdges
        public int selectedEdgeCount => m_SelectedEdges.Length;
        Edge[] m_SelectedEdges = new Edge[] { };

        //m_SelectedSharedVerticesCount
        internal int selectedSharedVerticesCount
        {
            get
            {
                CacheSelection();
                return m_SelectedSharedVerticesCount;
            }
        }
        int m_SelectedSharedVerticesCount = 0;

        //m_SelectedSharedVertices
        internal IEnumerable<int> selectedSharedVertices
        {
            get
            {
                CacheSelection();
                return m_SelectedSharedVertices;
            }
        }
        HashSet<int> m_SelectedSharedVertices = new HashSet<int>();

        //m_SelectedCoincidentVertices
        internal IEnumerable<int> selectedCoincidentVertices
        {
            get
            {
                CacheSelection();
                return m_SelectedCoincidentVertices;
            }
        }
        List<int> m_SelectedCoincidentVertices = new List<int>();

        //m_SelectedCacheDirty
        private void CacheSelection()
        {
            if (m_SelectedCacheDirty)
            {
                m_SelectedCacheDirty = false;
                m_SelectedSharedVertices.Clear();
                m_SelectedCoincidentVertices.Clear();
                var lookup = sharedVertexLookup;
                m_SelectedSharedVerticesCount = 0;

                foreach (var i in m_SelectedVertices)
                {
                    if (m_SelectedSharedVertices.Add(lookup[i]))
                    {
                        m_SelectedSharedVerticesCount++;
                        foreach (var n in sharedVertices[lookup[i]]) m_SelectedCoincidentVertices.Add(n);
                    }
                }
            }
        }
        private protected bool m_SelectedCacheDirty;

        //m_SharedVertexLookup
        public Dictionary<int, int> sharedVertexLookup
        {
            get
            {
                if ((m_CacheValid & CacheValidState.SharedVertex) != CacheValidState.SharedVertex)
                {
                    SharedVertex.GetSharedVertexLookup(m_SharedVertices, m_SharedVertexLookup);
                    m_CacheValid |= CacheValidState.SharedVertex;
                }
                return m_SharedVertexLookup;
            }
            set => m_SharedVertexLookup = value;
        }
        private protected Dictionary<int, int> m_SharedVertexLookup;

        public ReadOnlyCollection<int> selectedFaceIndexes => new ReadOnlyCollection<int>(m_SelectedFaces);
        public ReadOnlyCollection<int> selectedVertices => new ReadOnlyCollection<int>(m_SelectedVertices);
        public ReadOnlyCollection<Edge> selectedEdges => new ReadOnlyCollection<Edge>(m_SelectedEdges);

        private protected Edge[] selectedEdgesInternal => m_SelectedEdges;
        private protected int[] selectedIndexesInternal => m_SelectedVertices;
        private protected Face GetActiveFace() => selectedFaceCount < 1 ? null : faces[selectedFaceIndexes[selectedFaceCount - 1]];
        private protected Edge GetActiveEdge() => selectedEdgeCount < 1 ? Edge.Empty : m_SelectedEdges[selectedEdgeCount - 1];
        private protected int GetActiveVertex() => selectedVertexCount < 1 ? -1 : m_SelectedVertices[selectedVertexCount - 1];
        private protected void AddToFaceSelection(int index)
        {
            if (index > -1) SetSelectedFaces(m_SelectedFaces.Add(index));
        }
        private protected void SetSelectedFaces(IEnumerable<int> selected)
        {
            if (selected == null) ClearSelection();
            else
            {
                m_SelectedFaces = selected.ToArray();
                m_SelectedVertices = m_SelectedFaces.SelectMany(x => faces[x].distinctIndexesInternal).ToArray();
                m_SelectedEdges = m_SelectedFaces.SelectMany(x => faces[x].edges).ToArray();
            }
            m_SelectedCacheDirty = true;
            if (elementSelectionChanged != null) elementSelectionChanged(this);
        }
        private protected void RemoveFromFaceSelectionAtIndex(int index) => SetSelectedFaces(m_SelectedFaces.RemoveAt(index));

        public Face[] GetSelectedFaces()
        {
            int len = m_SelectedFaces.Length;
            var selected = new Face[len];
            for (var i = 0; i < len; i++) selected[i] = faces[m_SelectedFaces[i]];
            return selected;
        }
        public void SetSelectedFaces(IEnumerable<Face> selected)
        {
            SetSelectedFaces(selected != null ? selected.Select(x => Array.IndexOf(faces.ToArray(), x)) : null);
        }
        public void SetSelectedEdges(IEnumerable<Edge> edges)
        {
            if (edges == null) ClearSelection();
            else
            {
                m_SelectedFaces = new int[0];
                m_SelectedEdges = edges.ToArray();
                m_SelectedVertices = m_SelectedEdges.AllTriangles();
            }
            m_SelectedCacheDirty = true;
            if (elementSelectionChanged != null) elementSelectionChanged(this);
        }
        public void SetSelectedVertices(IEnumerable<int> vertices)
        {
            m_SelectedFaces = new int[0];
            m_SelectedEdges = new Edge[0];
            m_SelectedVertices = vertices != null ? vertices.Distinct().ToArray() : new int[0];
            m_SelectedCacheDirty = true;
            if (elementSelectionChanged != null) elementSelectionChanged(this);
        }
        public void ClearSelection()
        {
            m_SelectedFaces = new int[0];
            m_SelectedEdges = new Edge[0];
            m_SelectedVertices = new int[0];
            m_SelectedCacheDirty = true;
        }

        public static event Action<MeshInspectorProperty> elementSelectionChanged;

        public int faceCount => faces == null ? 0 : faces.Length;
        public int vertexCount => m_VerticesPositions == null ? 0 : m_VerticesPositions.Length;
        public int edgeCount => faces.Sum(x => x.edgesInternal.Length);
        public int indexCount => faces == null ? 0 : faces.Sum(x => x.indexesInternal.Length);
        public int triangleCount => faces == null ? 0 : faces.Sum(x => x.indexesInternal.Length) / 3;
    }
}

using UnityEngine;

namespace BlackEngine.MeshEditor.Runtime
{
    public class ActionResult
    {
        public enum Status
        {
            Success,
            Failure,
            Canceled,
            NoChange
        }

        public Status status { get; private set; }
        public string notification { get; private set; }

        public ActionResult(ActionResult.Status status, string notification)
        {
            this.status = status;
            this.notification = notification;
        }

        public static implicit operator bool(ActionResult res) => res != null && res.status == Status.Success;
        public bool ToBool() => status == Status.Success;
        public static bool FromBool(bool success) => success ? ActionResult.Success : new ActionResult(ActionResult.Status.Failure, "Failure");
        public static ActionResult Success => new ActionResult(ActionResult.Status.Success, "");
        public static ActionResult NoSelection => new ActionResult(ActionResult.Status.Canceled, "Nothing Selected");
        public static ActionResult UserCanceled => new ActionResult(ActionResult.Status.Canceled, "User Canceled");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct HandleAlignment
{
    public HandleAlignmentEnum handleAlignment
    {
        get => _handleAlignment;
        set
        {
            _handleAlignment = value;
            SetToActive();
        }
    }
    public enum HandleAlignmentEnum
    {
        Global = 1,
        Local = 2,
        Plane = 3,
        None = 0
    }

    public HandleAlignment(Transform transform)
    {
        _handleAlignmentObjects = new GameObject[3];
        _handleAlignmentObjects[(int)HandleAlignmentEnum.Global - 1] = transform.Find("Global").gameObject;
        _handleAlignmentObjects[(int)HandleAlignmentEnum.Local - 1] = transform.Find("Local").gameObject;
        _handleAlignmentObjects[(int)HandleAlignmentEnum.Plane - 1] = transform.Find("Plane").gameObject;
        _handleAlignment = HandleAlignmentEnum.None;
        SetToActive();
    }
    public void Next()
    {
        switch ((int)handleAlignment)
        {
            case 3: handleAlignment = HandleAlignmentEnum.None; break;
            default: handleAlignment = handleAlignment + 1; break;
        }
    }
    public void Prev()
    {
        switch ((int)handleAlignment)
        {
            case 0: handleAlignment = HandleAlignmentEnum.Plane; break;
            default: handleAlignment = handleAlignment - 1; break;
        }
    }

    private void SetToActive()
    {
        for (int i = 0; i < 3; i++) _handleAlignmentObjects[i].SetActive(i == (int)handleAlignment);
    }

    private GameObject[] _handleAlignmentObjects;
    private HandleAlignmentEnum _handleAlignment;
}

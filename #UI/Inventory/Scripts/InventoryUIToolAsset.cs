﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InventoryUIToolAsset : ScriptableObject
{
    public string itemName;
    public string itemTooltip;
    public string categoryName;
    public Sprite icon;
    public GameObject prefab;
    [HideInInspector] public GameObject gameObject;
}

﻿using UnityEngine;
using UnityEngine.Experimental.Input;

namespace BlackEngine.CharacterController
{
    public class PlayerInputControl : MonoBehaviour
    {
        public ExampleBaseController Character;
        public FollowCamera CharacterCamera;

        private void Update()
        {
            CharacterCamera.UpdateControl();
            HandleCharacterInput();

            if (Cursor.lockState == CursorLockMode.Locked && (Keyboard.current.tabKey.wasReleasedThisFrame || Keyboard.current.escapeKey.wasPressedThisFrame))
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else if (Cursor.lockState == CursorLockMode.None && Keyboard.current.tabKey.wasReleasedThisFrame)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        private void HandleCharacterInput()
        {
            PlayerCharacterInputs characterInputs = new PlayerCharacterInputs();
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                // Build the CharacterInputs struct
                characterInputs.MoveAxisForward = Keyboard.current.wKey.ReadValue() > 0 ? 1 : -Keyboard.current.sKey.ReadValue();
                characterInputs.MoveAxisRight = Keyboard.current.dKey.ReadValue() > 0 ? 1 : -Keyboard.current.aKey.ReadValue();
                characterInputs.CameraRotation = Camera.main.transform.rotation;//CharacterCamera.Transform.rotation;
                characterInputs.SprintHold = Keyboard.current.leftShiftKey.isPressed;
                //Jump
                characterInputs.JumpUp = Keyboard.current.spaceKey.wasReleasedThisFrame;
                characterInputs.JumpDown = Keyboard.current.spaceKey.wasPressedThisFrame;
                characterInputs.JumpHold = Keyboard.current.spaceKey.isPressed;
                //Crouch
                characterInputs.CrouchUp = Keyboard.current.cKey.wasReleasedThisFrame;
                characterInputs.CrouchDown = Keyboard.current.cKey.wasPressedThisFrame;
                characterInputs.CrouchHold = Keyboard.current.cKey.isPressed;
            }
            // Apply inputs to character
            Character.ApplyInputs(ref characterInputs);
        }
    }
}

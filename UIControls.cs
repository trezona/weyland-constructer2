﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Input;

public class UIControls : MonoBehaviour
{
    [SerializeField] private UIControlsConfig config = new UIControlsConfig();
    public static UIControls instance { get; private set; }

    [HideInInspector]
    public InventoryUIToolAsset assetInventory;
    
    void Awake()
    {
        instance = this;
        handleAlignment = new HandleAlignment(config.alignmentRect);
        toolbarUIControl = new ToolbarUIRender(config.toolbarRect);
    }
    void Update()
    {
        if (Keyboard.current.pKey.wasReleasedThisFrame) handleAlignment.Next();
        if (Keyboard.current.iKey.wasReleasedThisFrame) config.inventory.SetActive(!config.inventory.activeSelf);
        toolbarUIControl.UpdateKeys();
    }

    [Serializable]
    private struct UIControlsConfig
    {
        public Transform alignmentRect;
        public GameObject inventory;
        public Transform toolbarRect;
    }

    private HandleAlignment handleAlignment;
    private ToolbarUIRender toolbarUIControl;
}

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BlackEngine.MeshEditor.Runtime;
using BlackEngine.MeshEditor.Runtime.MeshOperations;
using UnityEngine;

namespace BlackEngine.MeshEditor
{
    /// <summary>
    /// This component is responsible for storing all the data necessary for editing and compiling UnityEngine.Mesh objects.
    /// </summary>
    [AddComponentMenu("")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class MeshInspector : MeshInspectorProperty
    {
        const int k_MeshFormatVersionSubmeshMaterialRefactor = 1;
        static HashSet<int> s_CachedHashSet = new HashSet<int>();
        const int k_UVChannelCount = 4; //Max number of UV channels that MeshInspector format supports.
        const int k_MeshFormatVersion = 1;// The current mesh format version. This is used to run expensive upgrade functions once in ToMesh().
        public const uint maxVertexCount = ushort.MaxValue;// The maximum number of vertices that a MeshInspector can accomodate.
        private protected int m_MeshFormatVersion = 1;

        /// <summary>
        /// In the editor, when a MeshEditor is destroyed it will also destroy the MeshFilter.sharedMesh that is found with the parent GameObject. You may override this behaviour by subscribing to onDestroyObject.
        /// </summary>
        /// <value>
        /// If onDestroyObject has a subscriber MeshEditor will invoke it instead of cleaning up unused meshes by itself.
        /// </value>
        /// <seealso cref="preserveMeshAssetOnDestroy"/>
        // public static event Action<MeshEditor> meshWillBeDestroyed;

        private void Awake()
        {
            if (vertexCount > 0 && faceCount > 0 && meshSyncState == MeshSyncState.Null) Rebuild();
        }
        private void GeometryWithPoints(Vector3[] points)
        {
            // Wrap in faces
            Face[] f = new Face[points.Length / 4];

            for (int i = 0; i < points.Length; i += 4)
            {
                f[i / 4] = new Face(new int[6]
                {
                    i + 0, i + 1, i + 2,
                    i + 1, i + 3, i + 2
                }, 0, AutoUnwrapSettings.tile, 0, -1, -1, false);
            }

            Clear();
            verticesPositions = points;
            faces = f;
            m_SharedVertices = SharedVertex.GetSharedVerticesWithPositions(points);
            InvalidateSharedVertexLookup();
            ToMesh();
            Refresh();
        }
        private void RefreshCollisions()
        {
            mesh.RecalculateBounds();
            var meshCollider = GetComponent<MeshCollider>();
            if (meshCollider != null)
            {
                gameObject.GetComponent<MeshCollider>().sharedMesh = null;
                gameObject.GetComponent<MeshCollider>().sharedMesh = mesh;
            }
        }
        private static bool IsValidTextureGroup(int group) => group > 0;
        private void RefreshColors()
        {
            Mesh m = meshFilter.sharedMesh;
            m.colors = m_Colors;
        }
        private void RefreshNormals()
        {
            Normals.CalculateNormals(this);
            mesh.normals = normals.ToArray();
        }
        private void RefreshTangents()
        {
            Normals.CalculateTangents(this);
            mesh.tangents = tangents;
        }

        private protected new void InvalidateSharedVertexLookup()
        {
            if (sharedVertexLookup == null) m_SharedVertexLookup = new Dictionary<int, int>();
            m_SharedVertexLookup.Clear();
            m_CacheValid &= ~CacheValidState.SharedVertex;
        }
        private protected new void InvalidateSharedTextureLookup()
        {
            if (m_SharedTextureLookup == null) m_SharedTextureLookup = new Dictionary<int, int>();
            m_SharedTextureLookup.Clear();
            m_CacheValid &= ~CacheValidState.SharedTexture;
        }
        public void InvalidateCaches()
        {
            InvalidateSharedVertexLookup();
            InvalidateSharedTextureLookup();
            foreach (var face in faces) face.InvalidateCache();
            m_SelectedCacheDirty = true;
        }
        public void SetSharedVertices(IEnumerable<KeyValuePair<int, int>> indexes)
        {
            if (indexes == null) throw new ArgumentNullException("indexes");
            m_SharedVertices = SharedVertex.ToSharedVertices(indexes);
            InvalidateSharedVertexLookup();
        }
        public void SetSharedTextures(IEnumerable<KeyValuePair<int, int>> indexes)
        {
            if (indexes == null) throw new ArgumentNullException("indexes");
            m_SharedTextures = SharedVertex.ToSharedVertices(indexes);
            InvalidateSharedTextureLookup();
        }
        private protected void MakeUnique()
        {
            // deep copy arrays of reference types
            sharedVertices = sharedVertices;
            SetSharedTextures(sharedTextureLookup);
            faces = faces.Select(x => new Face(x)).ToArray();

            // set a new UnityEngine.Mesh instance
            mesh = new Mesh();

            ToMesh();
            Refresh();
        }
        private protected int GetUnusedTextureGroup(int i = 1)
        {
            while (Array.Exists(faces.ToArray(), element => element.textureGroup == i)) i++;
            return i;
        }
        private protected int UnusedElementGroup(int i = 1)
        {
            while (Array.Exists(faces.ToArray(), element => element.elementGroup == i)) i++;
            return i;
        }
        public int GetSharedVertexHandle(int vertex)
        {
            int res;
            if (sharedVertexLookup.TryGetValue(vertex, out res)) return res;
            for (int i = 0; i < m_SharedVertices.Length; i++)
            {
                for (int n = 0, c = m_SharedVertices[i].Count; n < c; n++)
                    if (m_SharedVertices[i][n] == vertex) return i;
            }
            throw new ArgumentOutOfRangeException("vertex");
        }
        public HashSet<int> GetSharedVertexHandles(IEnumerable<int> vertices)
        {
            var lookup = sharedVertexLookup;
            HashSet<int> common = new HashSet<int>();
            foreach (var i in vertices) common.Add(lookup[i]);
            return common;
        }
        public List<int> GetCoincidentVertices(IEnumerable<int> vertices)
        {
            if (vertices == null) throw new ArgumentNullException("vertices");
            List<int> shared = new List<int>();
            GetCoincidentVertices(vertices, shared);
            return shared;
        }
        public void GetCoincidentVertices(IEnumerable<Face> faces, List<int> coincident)
        {
            if (faces == null) throw new ArgumentNullException("faces");
            if (coincident == null) throw new ArgumentNullException("coincident");
            coincident.Clear();
            s_CachedHashSet.Clear();
            var lookup = sharedVertexLookup;
            foreach (var face in faces)
            {
                foreach (var v in face.distinctIndexesInternal)
                {
                    var common = lookup[v];
                    if (s_CachedHashSet.Add(common))
                    {
                        var indices = m_SharedVertices[common];
                        for (int i = 0, c = indices.Count; i < c; i++) coincident.Add(indices[i]);
                    }
                }
            }
        }
        public void GetCoincidentVertices(IEnumerable<Edge> edges, List<int> coincident)
        {
            if (faces == null) throw new ArgumentNullException("edges");
            if (coincident == null) throw new ArgumentNullException("coincident");

            coincident.Clear();
            s_CachedHashSet.Clear();
            var lookup = sharedVertexLookup;

            foreach (var edge in edges)
            {
                var common = lookup[edge.a];
                if (s_CachedHashSet.Add(common))
                {
                    var indices = m_SharedVertices[common];
                    for (int i = 0, c = indices.Count; i < c; i++) coincident.Add(indices[i]);
                }
                common = lookup[edge.b];
                if (s_CachedHashSet.Add(common))
                {
                    var indices = m_SharedVertices[common];
                    for (int i = 0, c = indices.Count; i < c; i++) coincident.Add(indices[i]);
                }
            }
        }
        public void GetCoincidentVertices(IEnumerable<int> vertices, List<int> coincident)
        {
            if (vertices == null) throw new ArgumentNullException("vertices");
            if (coincident == null) throw new ArgumentNullException("coincident");

            coincident.Clear();
            s_CachedHashSet.Clear();
            var lookup = sharedVertexLookup;

            foreach (var v in vertices)
            {
                var common = lookup[v];
                if (s_CachedHashSet.Add(common))
                {
                    var indices = m_SharedVertices[common];
                    for (int i = 0, c = indices.Count; i < c; i++) coincident.Add(indices[i]);
                }
            }
        }
        public void GetCoincidentVertices(int vertex, List<int> coincident)
        {
            if (coincident == null) throw new ArgumentNullException("coincident");
            int common;
            if (!sharedVertexLookup.TryGetValue(vertex, out common)) throw new ArgumentOutOfRangeException("vertex");
            var indices = m_SharedVertices[common];
            for (int i = 0, c = indices.Count; i < c; i++) coincident.Add(indices[i]);
        }
        public void SetVerticesCoincident(IEnumerable<int> vertices)
        {
            var lookup = sharedVertexLookup;
            List<int> coincident = new List<int>();
            GetCoincidentVertices(vertices, coincident);
            SharedVertex.SetCoincident(ref lookup, coincident);
            SetSharedVertices(lookup);
        }
        public void SetTexturesCoincident(IEnumerable<int> vertices)
        {
            var lookup = sharedTextureLookup;
            SharedVertex.SetCoincident(ref lookup, vertices);
            SetSharedTextures(lookup);
        }
        public void AddToSharedVertex(int sharedVertexHandle, int vertex)
        {
            if (sharedVertexHandle < 0 || sharedVertexHandle >= m_SharedVertices.Length) throw new ArgumentOutOfRangeException("sharedVertexHandle");
            m_SharedVertices[sharedVertexHandle].Add(vertex);
            InvalidateSharedVertexLookup();
        }
        public void AddSharedVertex(SharedVertex vertex)
        {
            if (vertex == null) throw new ArgumentNullException("vertex");
            m_SharedVertices = m_SharedVertices.Add(vertex);
            InvalidateSharedVertexLookup();
        }
        public int id => gameObject.GetInstanceID();
        private protected MeshSyncState meshSyncState
        {
            get
            {
                if (mesh == null) return MeshSyncState.Null;
                int meshNo;
                int.TryParse(mesh.name.Replace("MeshEditor_", ""), out meshNo);
                if (meshNo != id) return MeshSyncState.InstanceIDMismatch;
                return mesh.uv2 == null ? MeshSyncState.Lightmap : MeshSyncState.InSync;
            }
        }
        private protected ReadOnlyCollection<Vector2> GetUVs(int channel)
        {
            if (channel == 0) return new ReadOnlyCollection<Vector2>(m_Textures0);
            if (channel == 1) return new ReadOnlyCollection<Vector2>(mesh.uv2);
            if (channel == 2) return m_Textures2 == null ? null : new ReadOnlyCollection<Vector2>(m_Textures2.Cast<Vector2>().ToList());
            if (channel == 3) return m_Textures3 == null ? null : new ReadOnlyCollection<Vector2>(m_Textures3.Cast<Vector2>().ToList());
            return null;
        }

        #region Create
        public static MeshInspector Create()
        {
            var go = new GameObject();
            var obj = go.AddComponent<MeshInspector>();
            obj.m_MeshFormatVersion = k_MeshFormatVersion;
            obj.Clear();
            return obj;
        }
        public static MeshInspector Create(Vector3[] positions)
        {
            if (positions.Length % 4 != 0)
            {
                Log.Warning("Invalid Geometry. Make sure vertices in are pairs of 4 (faces).");
                return null;
            }

            GameObject go = new GameObject();
            go.name = "MeshInspector";
            MeshInspector pb = go.AddComponent<MeshInspector>();
            pb.m_MeshFormatVersion = k_MeshFormatVersion;
            pb.GeometryWithPoints(positions);

            return pb;
        }
        public static MeshInspector Create(IEnumerable<Vector3> positions, IEnumerable<Face> faces)
        {
            GameObject go = new GameObject();
            MeshInspector pb = go.AddComponent<MeshInspector>();
            go.name = "MeshInspector";
            pb.m_MeshFormatVersion = k_MeshFormatVersion;
            pb.RebuildWithPositionsAndFaces(positions, faces);
            return pb;
        }
        public static MeshInspector Create(IList<Vertex> vertices, IList<Face> faces, IList<SharedVertex> sharedVertices = null, IList<SharedVertex> sharedTextures = null, IList<Material> materials = null)
        {
            var go = new GameObject();
            go.name = "MeshInspector";
            var mesh = go.AddComponent<MeshInspector>();
            if (materials != null) mesh.renderer.sharedMaterials = materials.ToArray();
            mesh.m_MeshFormatVersion = k_MeshFormatVersion;
            mesh.SetVertices(vertices);
            mesh.faces = faces.ToArray();
            mesh.sharedVertices = sharedVertices.ToArray();
            mesh.sharedTextures = sharedTextures != null ? sharedTextures.ToArray() : null;
            mesh.ToMesh();
            mesh.Refresh();
            return mesh;
        }
        #endregion

        public void SetFaceColor(Face face, Color color)
        {
            if (face == null) throw new ArgumentNullException("face");
            if (!HasArrays(MeshArrays.Color)) m_Colors = ArrayUtility.Fill(Color.white, vertexCount);
            foreach (int i in face.distinctIndexes) m_Colors[i] = color;
        }
        public void SetMaterial(IEnumerable<Face> faces, Material material)
        {
            var materials = renderer.sharedMaterials;
            var submeshCount = materials.Length;
            var index = -1;

            for (int i = 0; i < submeshCount && index < 0; i++)
            {
                if (materials[i] == material) index = i;
            }

            if (index < 0)
            {
                var submeshIndexes = new bool[submeshCount];
                foreach (var face in faces) submeshIndexes[MeshMath.Clamp(face.submeshIndex, 0, submeshCount - 1)] = true;
                index = Array.IndexOf(submeshIndexes, false);
                // Found an unused submeshIndex, replace it with the material.
                if (index > -1)
                {
                    materials[index] = material;
                    renderer.sharedMaterials = materials;
                }
                else
                {
                    // There were no unused submesh indices, append another submesh and material.
                    index = materials.Length;
                    var copy = new Material[index + 1];
                    Array.Copy(materials, copy, index);
                    copy[index] = material;
                    renderer.sharedMaterials = copy;
                }
            }
            foreach (var face in faces) face.submeshIndex = index;
        }
        public void RefreshUV(IEnumerable<Face> facesToRefresh)
        {
            // If the UV array has gone out of sync with the positions array, reset all faces to Auto UV so that we can
            // correct the texture array.
            if (!HasArrays(MeshArrays.Texture0))
            {
                m_Textures0 = new Vector2[vertexCount];
                foreach (Face f in faces) f.manualUV = false;
                facesToRefresh = faces;
            }

            s_CachedHashSet.Clear();

            foreach (var face in facesToRefresh)
            {
                if (face.manualUV) continue;
                int textureGroup = face.textureGroup;

                if (!IsValidTextureGroup(textureGroup)) UnwrappingUtility.Project(this, face);
                else if (!s_CachedHashSet.Add(textureGroup)) UnwrappingUtility.ProjectTextureGroup(this, textureGroup, face.uv);
            }
            mesh.uv = m_Textures0;

            if (HasArrays(MeshArrays.Texture2)) mesh.SetUVs(2, m_Textures2);
            if (HasArrays(MeshArrays.Texture3)) mesh.SetUVs(3, m_Textures3);
        }
        public void CopyFrom(MeshInspector other)
        {
            if (other == null) throw new ArgumentNullException("other");

            Clear();
            verticesPositions = other.verticesPositions;
            sharedVertices = other.sharedVertices;
            SetSharedTextures(other.sharedTextureLookup);
            faces = other.faces.Select(x => new Face(x)).ToArray();

            List<Vector4> uvs = new List<Vector4>();

            for (var i = 0; i < k_UVChannelCount; i++)
            {
                other.GetUVs(1, uvs);
                SetUVs(1, uvs);
            }

            userCollisions = other.userCollisions;
            tangents = other.tangents;
            colors = other.colors;
            selectable = other.selectable;
            unwrapParameters = new UnwrapParameters(other.unwrapParameters);
        }
        public void Refresh(RefreshMask mask = RefreshMask.All)
        {
            // Mesh
            if ((mask & RefreshMask.UV) > 0) RefreshUV(faces);
            if ((mask & RefreshMask.Colors) > 0) RefreshColors();
            if ((mask & RefreshMask.Normals) > 0) RefreshNormals();
            if ((mask & RefreshMask.Tangents) > 0) RefreshTangents();
            if ((mask & RefreshMask.Collisions) > 0) RefreshCollisions();
        }
        public void RebuildWithPositionsAndFaces(IEnumerable<Vector3> vertices, IEnumerable<Face> faces)
        {
            if (vertices == null) throw new ArgumentNullException("vertices");

            Clear();
            m_VerticesPositions = vertices.ToArray();
            faces = faces.ToArray();
            m_SharedVertices = SharedVertex.GetSharedVerticesWithPositions(m_VerticesPositions);
            InvalidateSharedVertexLookup();
            InvalidateSharedTextureLookup();
            ToMesh();
            Refresh();
        }
        public void Rebuild()
        {
            ToMesh();
            Refresh();
        }
        public void ToMesh(MeshTopology preferredTopology = MeshTopology.Triangles)
        {
            Mesh m = mesh;
            if (m != null && m.vertexCount == m_VerticesPositions.Length) m = mesh;
            else if (m == null) m = new Mesh();
            else m.Clear();

            m.vertices = m_VerticesPositions;
            m.uv2 = null;

            if (m_MeshFormatVersion < k_MeshFormatVersion)
            {
                if (m_MeshFormatVersion < k_MeshFormatVersionSubmeshMaterialRefactor) Submesh.MapFaceMaterialsToSubmeshIndex(this);
                m_MeshFormatVersion = k_MeshFormatVersion;
            }

            m_MeshFormatVersion = k_MeshFormatVersion;
            int materialCount = MeshUtility.GetMaterialCount(renderer);
            Submesh[] submeshes = Submesh.GetSubmeshes(faces, materialCount, preferredTopology);
            m.subMeshCount = materialCount;
            for (int i = 0; i < m.subMeshCount; i++) m.SetIndices(submeshes[i].m_Indexes, submeshes[i].m_Topology, i, false);
            m.name = string.Format("MeshInspector{0}", id);
            meshFilter.sharedMesh = m;
        }
        public void Clear()
        {
            faces = new Face[0];
            m_VerticesPositions = new Vector3[0];
            m_Textures0 = new Vector2[0];
            m_Textures2 = null;
            m_Textures3 = null;
            tangents = null;
            m_SharedVertices = new SharedVertex[0];
            m_SharedTextures = new SharedVertex[0];
            InvalidateSharedVertexLookup();
            InvalidateSharedTextureLookup();
            m_Colors = null;
            ClearSelection();
        }
        public void SetUVs(int channel, List<Vector4> uvs)
        {
            switch (channel)
            {
                case 0:
                    m_Textures0 = uvs != null ? uvs.Select(x => (Vector2)x).ToArray() : null;
                    break;

                case 1:
                    mesh.uv2 = uvs != null ? uvs.Select(x => (Vector2)x).ToArray() : null;
                    break;

                case 2:
                    m_Textures2 = uvs != null ? new List<Vector4>(uvs) : null;
                    break;

                case 3:
                    m_Textures3 = uvs != null ? new List<Vector4>(uvs) : null;
                    break;
            }
        }
        public bool HasArrays(MeshArrays channels)
        {
            bool missing = false;
            int vc = vertexCount;

            missing |= (channels & MeshArrays.Position) == MeshArrays.Position && m_VerticesPositions == null;
            missing |= (channels & MeshArrays.Normal) == MeshArrays.Normal && (normals == null || normals.Length != vc);
            missing |= (channels & MeshArrays.Texture0) == MeshArrays.Texture0 && (m_Textures0 == null || m_Textures0.Length != vc);
            missing |= (channels & MeshArrays.Texture2) == MeshArrays.Texture2 && (m_Textures2 == null || m_Textures2.Count != vc);
            missing |= (channels & MeshArrays.Texture3) == MeshArrays.Texture3 && (m_Textures3 == null || m_Textures3.Count != vc);
            missing |= (channels & MeshArrays.Color) == MeshArrays.Color && (m_Colors == null || m_Colors.Length != vc);
            missing |= (channels & MeshArrays.Tangent) == MeshArrays.Tangent && (tangents == null || tangents.Length != vc);

            // UV2 is a special case. It is not stored in MeshInspector, does not necessarily match the vertex count,
            // at it has a cost to check.
            if ((channels & MeshArrays.Texture1) == MeshArrays.Texture1)
            {
                var m_Textures1 = mesh != null ? mesh.uv2 : null;
                missing |= (m_Textures1 == null || m_Textures1.Length < 3);
            }
            return !missing;
        }
        public Vertex[] GetVertices(IList<int> indexes = null)
        {
            int meshVertexCount = vertexCount;
            int vc = indexes != null ? indexes.Count : vertexCount;

            Vertex[] v = new Vertex[vc];

            Vector3[] positions = verticesPositions;
            Vector2[] uv0s = textures;
            Vector4[] tangents = GetTangents();
            Vector3[] normals = GetNormals();
            Vector2[] uv2s = mesh != null ? mesh.uv2 : null;

            List<Vector4> uv3s = new List<Vector4>();
            List<Vector4> uv4s = new List<Vector4>();

            GetUVs(2, uv3s);
            GetUVs(3, uv4s);

            bool _hasPositions = positions != null && positions.Count() == meshVertexCount;
            bool _hasColors = colors != null && colors.Count() == meshVertexCount;
            bool _hasNormals = normals != null && normals.Count() == meshVertexCount;
            bool _hasTangents = tangents != null && tangents.Count() == meshVertexCount;
            bool _hasUv0 = uv0s != null && uv0s.Count() == meshVertexCount;
            bool _hasUv2 = uv2s != null && uv2s.Count() == meshVertexCount;
            bool _hasUv3 = uv3s.Count() == meshVertexCount;
            bool _hasUv4 = uv4s.Count() == meshVertexCount;

            for (int i = 0; i < vc; i++)
            {
                v[i] = new Vertex();
                int ind = indexes == null ? i : indexes[i];
                if (_hasPositions) v[i].position = positions[ind];
                if (_hasColors) v[i].color = colors[ind];
                if (_hasNormals) v[i].normal = normals[ind];
                if (_hasTangents) v[i].tangent = tangents[ind];
                if (_hasUv0) v[i].uv0 = uv0s[ind];
                if (_hasUv2) v[i].uv2 = uv2s[ind];
                if (_hasUv3) v[i].uv3 = uv3s[ind];
                if (_hasUv4) v[i].uv4 = uv4s[ind];
            }

            return v;
        }
        public void SetVertices(IList<Vertex> vertices, bool applyMesh = false)
        {
            if (vertices == null) throw new ArgumentNullException("vertices");
            var first = vertices.FirstOrDefault();
            if (first == null || !first.HasArrays(MeshArrays.Position))
            {
                Clear();
                return;
            }

            Vector3[] position;
            Color[] color;
            Vector3[] normal;
            Vector4[] tangent;
            Vector2[] uv0;
            Vector2[] uv2;
            List<Vector4> uv3;
            List<Vector4> uv4;

            Vertex.GetArrays(vertices, out position, out color, out uv0, out normal, out tangent, out uv2, out uv3, out uv4);

            m_VerticesPositions = position;
            m_Colors = color;
            tangents = tangent;
            m_Textures0 = uv0;
            m_Textures2 = uv3;
            m_Textures3 = uv4;

            if (applyMesh)
            {
                Mesh umesh = mesh;
                if (first.HasArrays(MeshArrays.Position)) umesh.vertices = position;
                if (first.HasArrays(MeshArrays.Color)) umesh.colors = color;
                if (first.HasArrays(MeshArrays.Texture0)) umesh.uv = uv0;
                if (first.HasArrays(MeshArrays.Normal)) umesh.normals = normal;
                if (first.HasArrays(MeshArrays.Tangent)) umesh.tangents = tangent;
                if (first.HasArrays(MeshArrays.Texture1)) umesh.uv2 = uv2;
                if (first.HasArrays(MeshArrays.Texture2)) umesh.SetUVs(2, uv3);
                if (first.HasArrays(MeshArrays.Texture3)) umesh.SetUVs(3, uv4);
            }
        }
        public Vector3[] GetNormals()
        {
            if (!HasArrays(MeshArrays.Normal)) Normals.CalculateNormals(this);
            return normals.ToArray();
        }
        public Color[] GetColors()
        {
            if (HasArrays(MeshArrays.Color)) return colors.ToArray();
            return ArrayUtility.Fill(Color.white, vertexCount);
        }
        public Vector4[] GetTangents()
        {
            if (!HasArrays(MeshArrays.Tangent)) Normals.CalculateTangents(this);
            return tangents.ToArray();
        }
        public void GetUVs(int channel, List<Vector4> uvs)
        {
            if (uvs == null) throw new ArgumentNullException("uvs");
            if (channel < 0 || channel > 3) throw new ArgumentOutOfRangeException("channel");
            uvs.Clear();

            switch (channel)
            {
                case 0:
                    for (int i = 0; i < vertexCount; i++) uvs.Add((Vector4)m_Textures0[i]);
                    break;

                case 1:
                    if (mesh != null && mesh.uv2 != null)
                    {
                        Vector2[] uv2 = mesh.uv2;
                        for (int i = 0; i < uv2.Length; i++) uvs.Add((Vector4)uv2[i]);
                    }
                    break;

                case 2:
                    if (m_Textures2 != null) uvs.AddRange(m_Textures2);
                    break;

                case 3:
                    if (m_Textures3 != null) uvs.AddRange(m_Textures3);
                    break;
            }
        }
    }
}

using UnityEngine;
using UnityEngine.Serialization;

namespace BlackEngine.MeshEditor.Runtime
{
    [System.Serializable]
    public struct AutoUnwrapSettings
    {
        public enum Anchor
        {
            UpperLeft,
            UpperCenter,
            UpperRight,
            MiddleLeft,
            MiddleCenter,
            MiddleRight,
            LowerLeft,
            LowerCenter,
            LowerRight,
            None
        }

        public enum Fill
        {
            Fit,
            Tile,
            Stretch
        }

        [SerializeField]
        [FormerlySerializedAs("useWorldSpace")]
        bool m_UseWorldSpace;

        [SerializeField]
        [FormerlySerializedAs("flipU")]
        bool m_FlipU;

        [SerializeField]
        [FormerlySerializedAs("flipV")]
        bool m_FlipV;

        [SerializeField]
        [FormerlySerializedAs("swapUV")]
        bool m_SwapUV;

        [SerializeField]
        [FormerlySerializedAs("fill")]
        Fill m_Fill;

        [SerializeField]
        [FormerlySerializedAs("scale")]
        Vector2 m_Scale;

        [SerializeField]
        [FormerlySerializedAs("offset")]
        Vector2 m_Offset;

        [SerializeField]
        [FormerlySerializedAs("rotation")]
        float m_Rotation;

        [SerializeField]
        [FormerlySerializedAs("anchor")]
        Anchor m_Anchor;

        public bool useWorldSpace
        {
            get => m_UseWorldSpace;
            set => m_UseWorldSpace = value;
        }

        public bool flipU
        {
            get => m_FlipU;
            set => m_FlipU = value;
        }

        public bool flipV
        {
            get => m_FlipV;
            set => m_FlipV = value;
        }

        /// <example>
        /// {U, V} becomes {V, U}
        /// </example>
        public bool swapUV
        {
            get => m_SwapUV;
            set => m_SwapUV = value;
        }

        public Fill fill
        {
            get => m_Fill;
            set => m_Fill = value;
        }

        public Vector2 scale
        {
            get => m_Scale;
            set => m_Scale = value;
        }

        public Vector2 offset
        {
            get => m_Offset;
            set => m_Offset = value;
        }

        public float rotation
        {
            get => m_Rotation;
            set => m_Rotation = value;
        }

        public Anchor anchor
        {
            get => m_Anchor;
            set => m_Anchor = value;
        }

        public AutoUnwrapSettings(AutoUnwrapSettings unwrapSettings)
        {
            m_UseWorldSpace = unwrapSettings.m_UseWorldSpace;
            m_FlipU = unwrapSettings.m_FlipU;
            m_FlipV = unwrapSettings.m_FlipV;
            m_SwapUV = unwrapSettings.m_SwapUV;
            m_Fill = unwrapSettings.m_Fill;
            m_Scale = unwrapSettings.m_Scale;
            m_Offset = unwrapSettings.m_Offset;
            m_Rotation = unwrapSettings.m_Rotation;
            m_Anchor = unwrapSettings.m_Anchor;
        }

        public static AutoUnwrapSettings tile
        {
            get
            {
                var res = new AutoUnwrapSettings();
                res.Reset();
                return res;
            }
        }

        public static AutoUnwrapSettings fit
        {
            get
            {
                var res = new AutoUnwrapSettings();
                res.Reset();
                res.fill = Fill.Fit;
                return res;
            }
        }

        public static AutoUnwrapSettings stretch
        {
            get
            {
                var res = new AutoUnwrapSettings();
                res.Reset();
                res.fill = Fill.Stretch;
                return res;
            }
        }

        public void Reset()
        {
            m_UseWorldSpace = false;
            m_FlipU = false;
            m_FlipV = false;
            m_SwapUV = false;
            m_Fill = Fill.Tile;
            m_Scale = new Vector2(1f, 1f);
            m_Offset = new Vector2(0f, 0f);
            m_Rotation = 0f;
            m_Anchor = Anchor.None;
        }

        public override string ToString()
        {
            string str =
                "Use World Space: " + useWorldSpace + "\n" +
                "Flip U: " + flipU + "\n" +
                "Flip V: " + flipV + "\n" +
                "Swap UV: " + swapUV + "\n" +
                "Fill Mode: " + fill + "\n" +
                "Anchor: " + anchor + "\n" +
                "Scale: " + scale + "\n" +
                "Offset: " + offset + "\n" +
                "Rotation: " + rotation;
            return str;
        }
    }
}

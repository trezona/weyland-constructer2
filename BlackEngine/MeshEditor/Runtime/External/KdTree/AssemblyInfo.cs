using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Unity.ProBuilder")]
[assembly: InternalsVisibleTo("UnityEngine.ProBuilder.MeshOperations")]

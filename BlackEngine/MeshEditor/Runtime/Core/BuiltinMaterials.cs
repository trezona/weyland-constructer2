using System;
using UnityEngine.Rendering;
using System.Reflection;
using UnityEngine;

namespace BlackEngine.MeshEditor
{
    public static class BuiltinMaterials
    {
        private static bool s_IsInitialized;
        public const string faceShader = "Hidden/ProBuilder/FaceHighlight";
        public const string lineShader = "Hidden/ProBuilder/LineBillboard";
        public const string pointShader = "Hidden/ProBuilder/PointBillboard";
        public const string wireShader = "Hidden/ProBuilder/FaceHighlight";
        public const string dotShader = "Hidden/ProBuilder/VertexShader";

        public static readonly Color previewColor = new Color(.5f, .9f, 1f, .56f);
        private static Shader s_SelectionPickerShader;
        private static bool s_GeometryShadersSupported;

        private static Material s_DefaultMaterial;
        private static Material s_FacePickerMaterial;
        private static Material s_VertexPickerMaterial;
        private static Material s_EdgePickerMaterial;
        private static Material s_UnityDefaultDiffuse;
        private static Material s_UnlitVertexColorMaterial;
        private static Material s_ShapePreviewMaterial;

        private static T Init<T>(T value)
        {
            if (s_IsInitialized) return value;
            s_IsInitialized = true;

            var geo = Shader.Find(lineShader);
            s_GeometryShadersSupported = geo != null && geo.isSupported;

            // ProBuilder default
            if (GraphicsSettings.renderPipelineAsset != null)
            {
                //s_DefaultMaterial = GraphicsSettings.renderPipelineAsset.defaultMaterial;//2019.1
                s_DefaultMaterial = GraphicsSettings.renderPipelineAsset.GetDefaultMaterial();
            }
            else
            {
                s_DefaultMaterial = (Material)Resources.Load("Materials/ProBuilderDefault", typeof(Material));
                if (s_DefaultMaterial == null || !s_DefaultMaterial.shader.isSupported) s_DefaultMaterial = GetLegacyDiffuse();
            }

            // SelectionPicker shader
            s_SelectionPickerShader = (Shader)Shader.Find("Hidden/ProBuilder/SelectionPicker");

            if ((s_FacePickerMaterial = Resources.Load<Material>("Materials/FacePicker")) == null)
            {
                Log.Error("FacePicker material not loaded... please re-install ProBuilder to fix this error.");
                s_FacePickerMaterial = new Material(Shader.Find("Hidden/ProBuilder/FacePicker"));
            }

            if ((s_VertexPickerMaterial = Resources.Load<Material>("Materials/VertexPicker")) == null)
            {
                Log.Error("VertexPicker material not loaded... please re-install ProBuilder to fix this error.");
                s_VertexPickerMaterial = new Material(Shader.Find("Hidden/ProBuilder/VertexPicker"));
            }

            if ((s_EdgePickerMaterial = Resources.Load<Material>("Materials/EdgePicker")) == null)
            {
                Log.Error("EdgePicker material not loaded... please re-install ProBuilder to fix this error.");
                s_EdgePickerMaterial = new Material(Shader.Find("Hidden/ProBuilder/EdgePicker"));
            }

            s_UnlitVertexColorMaterial = (Material)Resources.Load("Materials/UnlitVertexColor", typeof(Material));

            s_ShapePreviewMaterial = new Material(s_DefaultMaterial.shader);
            s_ShapePreviewMaterial.hideFlags = HideFlags.HideAndDontSave;

            if (s_ShapePreviewMaterial.HasProperty("_MainTex")) s_ShapePreviewMaterial.mainTexture = (Texture2D)Resources.Load("Textures/GridBox_Default");
            if (s_ShapePreviewMaterial.HasProperty("_Color")) s_ShapePreviewMaterial.SetColor("_Color", previewColor);
            return value;
        }

        public static bool geometryShadersSupported => Init<bool>(s_GeometryShadersSupported);
        public static Material defaultMaterial => Init<Material>(s_DefaultMaterial);
        public static Shader selectionPickerShader => Init<Shader>(s_SelectionPickerShader);
        public static Material facePickerMaterial => Init<Material>(s_FacePickerMaterial);
        public static Material vertexPickerMaterial => Init<Material>(s_VertexPickerMaterial);
        public static Material edgePickerMaterial => Init<Material>(s_EdgePickerMaterial);
        public static Material triggerMaterial => Init<Material>((Material)Resources.Load("Materials/Trigger", typeof(Material)));
        public static Material colliderMaterial => Init<Material>((Material)Resources.Load("Materials/Collider", typeof(Material)));
        public static Material GetLegacyDiffuse()
        {
            if (s_UnityDefaultDiffuse == null)
            {
                var mi = typeof(Material).GetMethod("GetDefaultMaterial", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
                if (mi != null) s_UnityDefaultDiffuse = mi.Invoke(null, null) as Material;

                if (s_UnityDefaultDiffuse == null)
                {
                    var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    s_UnityDefaultDiffuse = go.GetComponent<MeshRenderer>().sharedMaterial;
                    UnityEngine.Object.DestroyImmediate(go);
                }
            }
            return Init<Material>(s_UnityDefaultDiffuse);
        }
        public static Material unlitVertexColor => Init<Material>(s_UnlitVertexColorMaterial);
        public static Material ShapePreviewMaterial => Init<Material>(s_ShapePreviewMaterial);
    }
}

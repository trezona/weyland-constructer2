using System.Linq;
using System.Collections.Generic;
using BlackEngine.MeshEditor.Runtime;

namespace BlackEngine.MeshEditor
{
    static class EdgeUtility
    {
        internal static IEnumerable<Edge> GetSharedVertexHandleEdges(this MeshInspector mesh, IEnumerable<Edge> edges)
        {
            return edges.Select(x => GetSharedVertexHandleEdge(mesh, x));
        }
        internal static Edge GetSharedVertexHandleEdge(this MeshInspector mesh, Edge edge)
        {
            return new Edge(mesh.sharedVertexLookup[edge.a], mesh.sharedVertexLookup[edge.b]);
        }
        internal static Edge GetEdgeWithSharedVertexHandles(this MeshInspector mesh, Edge edge)
        {
            return new Edge(mesh.sharedVertices[edge.a][0], mesh.sharedVertices[edge.b][0]);
        }
        public static bool ValidateEdge(this MeshInspector mesh, Edge edge, out SimpleTuple<Face, Edge> validEdge)
        {
            IList<Face> faces = mesh.faces;
            SharedVertex[] sharedIndexes = mesh.sharedVertices;
            Edge universal = GetSharedVertexHandleEdge(mesh, edge);
            for (int i = 0; i < faces.Count; i++)
            {
                int dist_x = -1, dist_y = -1, shared_x = -1, shared_y = -1;
                if (faces[i].distinctIndexesInternal.ContainsMatch(sharedIndexes[universal.a].arrayInternal, out dist_x, out shared_x) &&
                    faces[i].distinctIndexesInternal.ContainsMatch(sharedIndexes[universal.b].arrayInternal, out dist_y, out shared_y))
                {
                    int x = faces[i].distinctIndexesInternal[dist_x];
                    int y = faces[i].distinctIndexesInternal[dist_y];

                    validEdge = new SimpleTuple<Face, Edge>(faces[i], new Edge(x, y));
                    return true;
                }
            }
            validEdge = new SimpleTuple<Face, Edge>();
            return false;
        }
        public static bool Contains(this Edge[] edges, Edge edge)
        {
            for (int i = 0; i < edges.Length; i++)
            {
                if (edges[i].Equals(edge)) return true;
            }
            return false;
        }
        public static bool Contains(this Edge[] edges, int x, int y)
        {
            for (int i = 0; i < edges.Length; i++)
            {
                if ((x == edges[i].a && y == edges[i].b) || (x == edges[i].b && y == edges[i].a)) return true;
            }
            return false;
        }
        public static int IndexOf(this MeshInspector mesh, IList<Edge> edges, Edge edge)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                if (edges[i].Equals(edge, mesh.sharedVertexLookup)) return i;
            }
            return -1;
        }
        public static int[] AllTriangles(this Edge[] edges)
        {
            int[] arr = new int[edges.Length * 2];
            int n = 0;
            for (int i = 0; i < edges.Length; i++)
            {
                arr[n++] = edges[i].a;
                arr[n++] = edges[i].b;
            }
            return arr;
        }
        public static Face GetFace(this MeshInspector mesh, Edge edge)
        {
            Face res = null;
            foreach (var face in mesh.faces)
            {
                var edges = face.edgesInternal;
                for (int i = 0, c = edges.Length; i < c; i++)
                {
                    if (edge.Equals(edges[i])) return face;
                    if (edges.Contains(edges[i])) res = face;
                }
            }
            return res;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class UIElementsRender : MonoBehaviour
{
    public Transform parentObject;
    public GameObject itemLayout;
    public Texture[] iconElements;

    private int i = 0;
    private void Start()
    {
        while (i < iconElements.Length)
        {
            GameObject tmpObject = Instantiate(itemLayout, parentObject);
            (tmpObject.GetComponentInChildren(typeof(Text)) as Text).text = tmpObject.name = iconElements[i].name;
            (tmpObject.GetComponentInChildren(typeof(RawImage)) as RawImage).texture = iconElements[i];
            (tmpObject.GetComponentInChildren(typeof(Button)) as Button).onClick.AddListener(() => Test(tmpObject));
            i++;
        }
        Destroy(this);
    }

    void Test(GameObject Button) { Debug.Log(Button); }
}
